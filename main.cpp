#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include <sys/time.h>
#include "x86intrin.h"
#include <math.h> 

using namespace std;

#define SIZE 1048576

typedef union {
	__m128 			float128;	
	float		m128_32[4];
} floatVec;

typedef union {
	__m128i 			int128;
	int		m128i_32[4];
} intVec;

float* random_generator() { 
	float* arr = new float[SIZE];   
	for(int i=0; i<SIZE; i++)
		arr[i] = float(rand())/float((RAND_MAX)) * 100 + 100;
	return arr; 
} 
  
long parallel_run(float * random_numbers, bool print) {
	struct timeval start, end;
	__m128 vec;
	__m128 sqr;
	float mean;
	float mean_sqr_sum;
	float result;

	gettimeofday(&start, NULL);

	__m128 sum = _mm_setzero_ps();
	__m128 sqr_sum = _mm_setzero_ps();
	for(long i=0; i<SIZE; i=i+4) {
		vec = _mm_loadu_ps(&random_numbers[i]);
		sum = _mm_add_ps(sum, vec);
		sqr = _mm_mul_ps(vec, vec);
		sqr_sum = _mm_add_ps(sqr_sum, sqr);
	}
	sum = _mm_hadd_ps(sum, sum);
	sum = _mm_hadd_ps(sum, sum);
	mean = _mm_cvtss_f32(sum)/SIZE;
	sqr_sum = _mm_hadd_ps(sqr_sum, sqr_sum);
	sqr_sum = _mm_hadd_ps(sqr_sum, sqr_sum);
	mean_sqr_sum = _mm_cvtss_f32(sqr_sum)/SIZE;
	result = sqrt(mean_sqr_sum-(mean*mean));

	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

	if (print == true) {
		printf("\nParallel:\n");
		printf("Average: %f\n", mean);
		printf("Result: %f\n", result);
		printf("Time: %ld microsecond\n", micros);		
	}

	return micros;
}

int serial_run(float * random_numbers, bool print) {
	struct timeval start, end;
	float mean;
	float result;
	float mean_sqr_sum;

	gettimeofday(&start, NULL);

	float sum_sqr[4] = {0.0, 0.0, 0.0, 0.0};
	float sum[4] = {0.0, 0.0, 0.0, 0.0};
	for (int j=0; j<4; j++) {
		for(long i=0; i<SIZE; i=i+4) {
			sum[j] = sum[j] + random_numbers[i+j];
			sum_sqr[j] = sum_sqr[j] + ((random_numbers[i+j])*(random_numbers[i+j]));
		}		
	}
	mean = ((sum[0] + sum[1])+(sum[2] + sum[3]))/SIZE;
	mean_sqr_sum = ((sum_sqr[0] + sum_sqr[1])+(sum_sqr[2] + sum_sqr[3]))/SIZE;
	result = sqrt(mean_sqr_sum - ((mean)*(mean)));

	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

	if (print == true) {
		printf("\nSerial:\n");
		printf("Average: %f\n", mean);
		printf("Result: %f\n", result);
		printf("Time: %ld microsecond\n", micros);		
	}

	return micros;	
}

float part2(bool print) {
	float* r = random_generator();
	long pTime = parallel_run(r, print); 
	long sTime = serial_run(r, print);
	return float(sTime)/float(pTime);
}

long part1_parallel(float * random_numbers, bool print) {
	struct timeval start, end;
	floatVec vec;
	floatVec compare;
	floatVec max;
	intVec index;

	gettimeofday(&start, NULL);

	max.float128 = _mm_loadu_ps(&random_numbers[0]);
	index.int128 = _mm_set_epi32(3, 2, 1, 0);

	for(long i=4; i<SIZE; i=i+4) {
		vec.float128 = _mm_loadu_ps(&random_numbers[i]);
		compare.float128 = _mm_cmpgt_ps(vec.float128, max.float128);
		if(compare.m128_32[0] != 0.0) {
			max.m128_32[0] = vec.m128_32[0];
			index.m128i_32[0] = i;
		}
		if(compare.m128_32[1] != 0.0) {
			max.m128_32[1] = vec.m128_32[1];
			index.m128i_32[1] = i+1;
		}
		if(compare.m128_32[2] != 0.0) {
			max.m128_32[2] = vec.m128_32[2];
			index.m128i_32[2] = i+2;
		}
		if(compare.m128_32[3] != 0.0) {
			max.m128_32[3] = vec.m128_32[3];
			index.m128i_32[3] = i+3;
		}
	}

	// for(long i=4; i<SIZE; i=i+4) {
	// 	vec.float128 = _mm_loadu_ps(&random_numbers[i]);
	// 	max.float128 = _mm_max_ps(vec.float128, max.float128);
	// 	if(max.m128_32[0] == vec.m128_32[0]) {
	// 		index.m128i_32[0] = i;
	// 	}
	// 	if(max.m128_32[1] == vec.m128_32[1]) {
	// 		index.m128i_32[1] = i+1;
	// 	}
	// 	if(max.m128_32[2] == vec.m128_32[2]) {
	// 		index.m128i_32[2] = i+2;
	// 	}
	// 	if(max.m128_32[3] == vec.m128_32[3]) {
	// 		index.m128i_32[3] = i+3;
	// 	}
	// }

	float maximum_num = max.m128_32[0];
	long maximum_ind = index.m128i_32[0];
	for (int k=1; k<4; k++)
	{
		if (max.m128_32[k] > maximum_num) {
			maximum_num = max.m128_32[k];
			maximum_ind = index.m128i_32[k];
		}
	}
	
	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

	if (print == true) {
		printf("\nParallel:\n");
		printf("Maximum: %f , Index: %ld\n", maximum_num, maximum_ind);
		printf("Time: %ld microsecond\n", micros);
	}
	
	return micros;
}

long part1_serial(float * random_numbers, bool print) {
	struct timeval start, end;
	float max;
	long index;
	
	gettimeofday(&start, NULL);

	max = random_numbers[0];
	index = 0;

	for (long j=1; j<SIZE; j++) {
		if (random_numbers[j] > max) {
			max = random_numbers[j];
			index = j;
		}
	}

	gettimeofday(&end, NULL);
	long micros = ((end.tv_sec - start.tv_sec) * 1000000) + (end.tv_usec - start.tv_usec);

	if (print == true) {
		printf("\nSerial:\n");
		printf("Maximum: %f , Index: %ld\n", max, index);
		printf("Time: %ld microsecond\n", micros);
	}

	return micros;
}

float part1(bool print) {
	float* r = random_generator();
	long pTime = part1_parallel(r, print); 
	long sTime = part1_serial(r, print);
	return float(sTime)/float(pTime);
}

int main() { 
	printf("MirHamed Jafarzadeh Asl: 810096008\n");
	printf("Seyede Mehrnaz Shamsabadi: 810196493\n");

	printf("**************************************\n");

	printf("Part 1:\n");
	float average_speed_up_part1 = part1(true);
	for (int i=1; i<10; i++)
		average_speed_up_part1 = average_speed_up_part1 + part1(false);
	printf("\nAverage Speed Up of Part 1: %f\n", average_speed_up_part1/10);

	printf("**************************************\n");

	printf("Part 2:\n");
	float average_speed_up_part2 = part2(true);
	for (int i=1; i<10; i++)
		average_speed_up_part2 = average_speed_up_part2 + part2(false);
	printf("\nAverage Speed Up of Part 2: %f\n", average_speed_up_part2/10);
	
	return 0; 
} 